import matplotlib.pyplot as plt
import numpy as np

from konlpy.tag import Mecab
from kss import split_sentences

m = Mecab(dicpath="C:/mecab/mecab-ko-dic") # 사전 저장 경로에 자신이 mecab-ko-dic를 저장한 위치를 적는다. (default: "/usr/local/lib/mecab/dic/mecab-ko-dic")

def get_morpheme_splited_sentence(documents: dict):
    morpheme_sentence_list_by_id = {}
    for document in documents:
        morpheme_sentence_list_by_id[document['DOCID']] = ([m.morphs(sentence) for sentence in split_sentences(document['CONTENT'])])
    return morpheme_sentence_list_by_id

def get_morpheme_sentence(documents: dict):
    morpheme_sentence_list_by_id = {}
    for document in documents:
        morpheme_sentence_list_by_id[document['DOCID']] = ([m.morphs(document['CONTENT'])])
    return morpheme_sentence_list_by_id


def get_tf(morpheme_sentence_list_by_id: dict):
    tf_all = {}
    for key in morpheme_sentence_list_by_id:
        sentence_list = morpheme_sentence_list_by_id[key]
        tf = {}
        for morpheme_sentence in sentence_list:
            for morpheme in morpheme_sentence:
                if morpheme not in tf:
                    tf[morpheme] = 1
                else :
                    tf[morpheme] += 1
        tf = {k: v for k, v in sorted(tf.items(), key=lambda item: (-item[1], item[0]))}
        tf_all[key] = tf
    return  tf_all

from scipy import interpolate
def interpolate_1(y):
    x = np.linspace(0, 2*np.pi, len(y))
    xnew = np.linspace(0, 2*np.pi,  1000)
    f_prev = interpolate.interp1d(x, y, kind='quadratic') # next
    y_new_prev = f_prev(xnew)
    return xnew, y_new_prev

# from method import *
import matplotlib.pyplot as plt
import numpy as np

def fft_result(signal):
    fft = np.fft.rfft(signal) / len(signal)
    amplitude = np.abs(fft)
    phase = np.angle(fft)
    frequency = np.fft.fftfreq(len(fft), len(signal))
    return frequency, amplitude, phase

def first_fft_ifft(signal):
    signal = signal / np.max(signal)
    t, signal = interpolate_1(signal)

    plt.subplot(3,1,1)
    plt.plot(t,signal)
    plt.grid()

    fft = np.fft.rfft(signal) / len(signal)
    fft[0] = 0
    amplitude = abs(fft)
    # print(amplitude)
    frequency = np.fft.fftfreq(len(fft), len(signal))
    plt.subplot(3,1,2)
    plt.plot( amplitude)
    plt.grid()

    plt.subplot(3,1,3)

    amplitude = np.array([1 if i > 0.001 else 0 for i in amplitude])
    fft = fft * amplitude
    ifft = np.fft.irfft(fft)
    plt.plot( ifft )
    plt.grid()
    plt.show()
    print(len(amplitude))
    #
    #return signal, frequency, fft

def normalize(signal):
    fft = np.fft.fft(signal) / len(signal)
    fft[0] = 0
    return np.fft.ifft(fft)


def PositiveFFT(x, Fs, w=None):
    Ts = 1 / Fs
    Nsamp = x.size

    xFreq = np.fft.rfftfreq(Nsamp, Ts)[:-1]

    if w is None:
        yFFT = (np.fft.rfft(x) / Nsamp)[:-1] * 2
    else:
        yFFT = (np.fft.rfft(x * w) / Nsamp)[:-1] * 2

    return xFreq, yFFT

import math

def fft(signal):
    n=len(signal)        # Length of signal
    NFFT = n      # ?? NFFT=2^nextpow2(length(y))  ??
    k=np.arange(NFFT)
    f0=k * n / NFFT    # double sides frequency range
    f0=f0[range(math.trunc(NFFT/2))]        # single sied frequency range

    Y =np.fft.fft(signal)/NFFT        # fft computing and normaliation
    Y =Y[range(math.trunc(NFFT/2))]          # single sied frequency range
    Y[0] = 0
    amplitude_Hz = 2*abs(Y)
    phase_ang = np.angle(Y)*180/np.pi

    plt.subplot(4,1,1)
    plt.stem(signal)   #  2* ???
    plt.grid()

    plt.subplot(4,1,2)
    # Plot single-sided amplitude spectrum.

    plt.stem(f0,amplitude_Hz,'r')   #  2* ???
    plt.xlabel('frequency($Hz$)')
    plt.ylabel('amplitude')
    plt.grid()

    # Phase ....
    plt.subplot(4,1,3)
    plt.stem(f0,phase_ang,'r')   #  2* ???
    plt.xlabel('frequency($Hz$)')
    plt.ylabel('phase($deg.$)')
    plt.grid()

    #plt.figure(num=2,dpi=100,facecolor='white')
    plt.subplot(4,1,4)
    result = np.fft.ifft(Y)
    plt.stem(result)   #  2* ???
    plt.grid()

    plt.savefig("./test_figure2.png",dpi=300)
    plt.show()

import scipy.stats as stats

def correlate(signal1, signal2):
    signal1 = signal1 - np.ones(len(signal1)) * np.mean(signal1)
    signal2 = signal2 - np.ones(len(signal2)) * np.mean(signal2)
    corr = np.corrcoef(signal1, signal2)[0,1]
    # print(len(signal1))
    # print(len(signal2))
    # print(corr)
    plt.plot()
    plt.scatter(signal1, signal2)
    plt.grid()

    # plt.subplot(2,1,2)
    # plt.stem(corr)
    # plt.grid()
    # plt.show()
    return stats.pearsonr(signal1,signal2)

def conv(signal1, signal2):
    conv = np.convolve(signal1, signal2)
    print(len(signal1))
    print(len(signal2))
    print(len(conv))
    plt.stem(conv)
    plt.grid()
    plt.show()